﻿#include <iostream>
#include <string>
using namespace std;


class Player{
public:
    void Print() 
    {
        cout << name << " : " << points << endl;
    }

    void SetPlayer() {
        cout << "Введите имя игрока: ";
        cin >> name;
        cout << "Введите очки игрока: ";
        cin >> points;
    }

    int Getpoints()
    {
        return points;
    }

private:
    string name;
    int points;
};

class MassPlayer {
public:
    MassPlayer()
    {
        QuantityPlayers = 0;
    }

    ~MassPlayer()
    {
        delete[] players;
    }

    void Add() 
    {
        cout << "укажите количество играков: ";
        cin >> QuantityPlayers;
        players = new Player[QuantityPlayers];
        for (int i = 0; i < QuantityPlayers; i++) 
        {
            cout << "\nИгрок " << i + 1 << endl;
            players[i].SetPlayer();
        }
    }

    void Print()
    {
        for (int i = 0; i < QuantityPlayers; i++)
        {
            players[i].Print();
        }
    }

    void Sort()
    {
        int length = QuantityPlayers;

        while (length--)
        {
            bool swapped = false;
            for (int i = 0; i < length; i++)
            {
                if (players[i].Getpoints() < players[i + 1].Getpoints())
                {
                    swap(players[i], players[i + 1]);
                    swapped = true;
                }
            }

            if (swapped == false)
                break;
        }
    }
private:
    int QuantityPlayers;
    Player* players;
};

int main()
{
    setlocale(LC_ALL, ("ru"));

    MassPlayer plauers;
    plauers.Add();
    cout << "\nВывод игроков: " << endl;
    plauers.Print();
    plauers.Sort();
    cout << "\nВывод отсортированных игроков: " << endl;
    plauers.Print();

    return 0;
}


